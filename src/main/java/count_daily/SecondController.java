package count_daily;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@Named
@ApplicationScoped
public class SecondController {

    @Inject
    Booking booking;

    public static Statement stmtObj;
    public static Connection connObj;
    public static ResultSet resultSetObj;
    private static int totalRecords;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    public DateFormat getDate() {
        return dateFormat;
    }

    public void setDate(DateFormat date) {
        this.dateFormat = date;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public static Connection getConnection(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            String db_url ="jdbc:mysql://27.74.247.203:33066/medprodb", db_userName = "root", db_password = "abc123!!!";
            connObj = DriverManager.getConnection(db_url,db_userName,db_password);
        } catch(Exception sqlException) {
            sqlException.printStackTrace();
        }
        return connObj;
    }

    /*@PostConstruct
    public void init() { // retrieve from current day
        booking.setListFromDB(getListFromSP("2020-10-30"));
    }*/

  /*  public ArrayList<Booking> getListByDate(){
        String sql = "select * from Booking where Booking_date = '" + date + "%'";
        return getListFromDB(sql);
    }

    public static ArrayList<Booking> getListFromDB(String sql) {
        ArrayList<Booking> list = new ArrayList<>();
        try {
            stmtObj = getConnection().createStatement();
            resultSetObj = stmtObj.executeQuery(sql);
            while(resultSetObj.next()) {
                Booking Booking = new Booking();
                Booking.setId(resultSetObj.getInt("id"));
                Booking.setApp(resultSetObj.getString("app"));
                Booking.setBooking_date(resultSetObj.getDate("Booking_date"));
                Booking.setBooking_phone(resultSetObj.getString("Booking_phone"));
                Booking.setBooking_number(resultSetObj.getInt("Booking_number"));
                list.add(Booking);
            }
            totalRecords = list.size();
            System.out.println("Total Records Fetched: " + list.size());
            connObj.close();
        } catch(Exception sqlException) {
            sqlException.printStackTrace();
        }
        return list;
    }
*/
    public boolean validate_booking_date(){
        String date = dateFormat.format(booking.getBooking_date());
        try {
            if(!date.equals("") || date.matches("^([12][0-9]{3})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$")){
                System.out.println("Booking_day: " + booking.getBooking_date());
                return true;
            }
        } catch (Exception e){
            System.out.println("Not found booking day or incorrect date format");
        }
        return false;
    }

    public String submit() throws SQLException {
        if(validate_booking_date()){
             booking.setListFromDB(getListFromSP(dateFormat.format(booking.getBooking_date())));
        }
        return "count_daily.xhtml?faces-redirect=true";
    }
    // fetch all
   /* public static void fetchAll(){
        try {
            getConnection();
            stmtObj = connObj.createStatement();
            ResultSet resultSet = stmtObj.executeQuery("select * from Booking");
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            for(int i=1; i <= resultSetMetaData.getColumnCount(); i++){
                System.out.println(resultSetMetaData.getColumnLabel(i));
            }
            System.out.println(resultSetMetaData.getColumnCount());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void main(String[] args) {
        fetchAll();
    }*/

// session 2 ---------------------------------------------------------
    // get specific data from store procedure
    public ArrayList<Booking> getListFromSP(String p_date) {
        ArrayList<Booking> list = new ArrayList<Booking>();
        try {
          String sql = "{call count_daily_2(?)}";
            CallableStatement statement = getConnection().prepareCall(sql);
            //statement.setString(1, "2019-08-24");
            statement.setString(1, p_date);
            resultSetObj = statement.executeQuery();
            while (resultSetObj.next()){
                Booking booking = new Booking();
                booking.setKey(resultSetObj.getString(1));
                booking.setValue(resultSetObj.getInt(2));

                list.add(booking);
            }
            totalRecords = list.size();
            System.out.print("Total record: " + list.size());
            booking.setNumber(list.get(0).getValue());
            connObj.close();
        } catch (SQLException e){
            e.getErrorCode();
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        getConnection();
        SecondController secondController = new SecondController();
        secondController.getListFromSP("2020-10-30");
    }



}
