package count_daily;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@Named("main_booking")
@ApplicationScoped
public class Booking {

    private String key;
    private int value;
    private Date booking_date;
    int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(Date booking_date) {
        this.booking_date = booking_date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    //------------------------------------------------------------------

    public ArrayList<Booking> getListFromDB() {
        return listFromDB;
    }

    public void setListFromDB(ArrayList<Booking> listFromDB) {
        this.listFromDB = listFromDB;
    }

    private ArrayList<Booking> listFromDB;

    public ArrayList<Booking> list(){
        return listFromDB;
    }






}
